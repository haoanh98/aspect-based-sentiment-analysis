# Aspect-Based Sentiment Analysis

Aspect-based sentiment analysis (ABSA) task is a multi-grained task of natural language processing and consists of two subtasks: Aspect Category Detection and Aspect Polarity Classification in Vietnamese.

# Dataset 

## UIT-ViSFD: A Vietnamese Smartphone Feedback Dataset for Aspect-Based Sentiment Analysis

UIT-ViSFD consists of 11,122 human-annotated comments for mobile e-commerce, which is freely available for research purposes: 

* Link paper: [SA2SL: From Aspect-Based Sentiment Analysis to Social Listening System for Business Intelligence](https://link.springer.com/chapter/10.1007/978-3-030-82147-0_53)
* Link dataset: [Vietnamese Smartphone Feedback Dataset](https://github.com/LuongPhan/UIT-ViSFD)


## UIT ABSA Datasets

* Link paper: [Two New Large Corpora for Vietnamese Aspect-based Sentiment Analysis at Sentence Level](https://dl.acm.org/doi/abs/10.1145/3446678)

**Restaurant Dataset**: 7028 reviews (train), 771 reviews (development), 1938 reviews (test)

**Hotel Dataset**: 7180 reviews (train), 795 reviews (development), 2030 reviews (test)

## VLSP 2018 Shared Task: Aspect Based Sentiment Analysis

* Link paper: [VLSP 2018 Shared Task: Aspect Based Sentiment Analysis Paper](https://drive.google.com/file/d/1C3l2n2Jicwzc0aoBLoyA5zIdczHpcy5R/view?usp=sharing)

### Leaderboard

**Restaurant Dataset**: 2961 reviews (train), 1290 reviews (development), 500 reviews (test) 

<table>
  <tr>
    <th>Model</th>
    <th>Aspect (F1)</th>
    <th>Aspect-Polarity (F1)</th>
    <th>Paper/Source</th>
    <th>Code</th>
  </tr>
  <tr>
    <td>CNNs</td>
    <td>0.80</td>
    <td></td>
    <td>
      <a href="https://drive.google.com/file/d/1mioRMEkPzkk9V_aDZgBaEG1vxWVd4iT1/view?usp=sharing">Dang et al. NICS'18</a>
    </td>
    <td></td>
  </tr>
  <tr>
    <td>SVM</td>
    <td>0.77</td>
    <td>0.61</td>
    <td>
      <a href="https://drive.google.com/file/d/1I2U2AinR5kfz1gjZRgfgsRQwqZEo7-od/view?usp=sharing">Dang et al. VLSP'18</a>
    </td>
    <td></td>
  </tr>
  <tr>
    <td>SVM</td>
    <td>0.54</td>
    <td>0.48</td>
    <td>
      <a href="https://drive.google.com/file/d/1-3HYFHjDv1R-H5HOIC9es1Xd-gBEtcZ9/view?usp=sharing">Nguyen et al. VLSP'18</a>
    </td>
    <td></td>
  </tr>
</table>

**Hotel Dataset**: 3000 reviews (training), 2000 reviews (development), 600 reviews (test)

<table>
  <tr>
    <th>Model</th>
    <th>Aspect (F1)</th>
    <th>Aspect-Polarity (F1)</th>
    <th>Paper/Source</th>
    <th>Code</th>
  </tr>
  <tr>
    <td>SVM</td>
    <td>0.70</td>
    <td>0.61</td>
    <td><a href="https://drive.google.com/file/d/1I2U2AinR5kfz1gjZRgfgsRQwqZEo7-od/view?usp=sharing">Dang et al. VLSP'18</a>
    </td>
    <td></td>
  </tr>
  <tr>
    <td>CNNs</td>
    <td>0.69</td>
    <td></td>
    <td>
      <a href="https://drive.google.com/file/d/1mioRMEkPzkk9V_aDZgBaEG1vxWVd4iT1/view?usp=sharing">Dang et al. NICS'18</a>
    </td>
    <td></td>
  </tr>
  <tr>
    <td>SVM</td>
    <td>0.56</td>
    <td>0.53</td>
    <td><a href="https://drive.google.com/file/d/1-3HYFHjDv1R-H5HOIC9es1Xd-gBEtcZ9/view?usp=sharing">Nguyen et al. VLSP'18</a></td>
    <td></td>
  </tr>
</table>

# Our Approach
<p align="center">
	<img src="model.png">
</p>

# Enviroment

Enviroment python >=3.7.11.

```
pip install tensorflow
pip install tensorflow_addons
pip install transformers
pip install gensim==3.6.0
pip install pandas
pip install numpy
pip install vncorenlp
pip install fairseq
pip install fastBPE
```

# Guide 

Run project in [Colab](https://colab.research.google.com/drive/1B_NcM7YgyMIFijOXdvpsQQmNuxZ3OFCr#scrollTo=giRxJBa-wRK7)

# Result

**F1-score on test set**

<table>
  <tr>
    <th>Dataset </th>
    <th>Aspect (F1)</th>
    <th>Aspect-Polarity (F1)</th>
  </tr>
  <tr>
    <td>UIT-ViSFD</td>
    <td>86.58(macro)</td>
    <td>72.91(macro)</td>
  </tr>
  <tr>
    <td>UIT-ABSA</td>
    <td>87.63/79.72</td>
    <td>75.44/74.46</td>
  </tr>
  <tr>
    <td>VLSP_2018</td>
    <td>83.57/83.37</td>
    <td>71.52/75.72</td>
  </tr>
</table>

**F1-score on development set**

<table>
  <tr>
    <th>Dataset </th>
    <th>Aspect (F1)</th>
    <th>Aspect-Polarity (F1)</th>
  </tr>
  <tr>
    <td>UIT-ViSFD</td>
    <td>85.68(macro)</td>
    <td>74.47(macro)</td>
  </tr>
  <tr>
    <td>UIT-ABSA</td>
    <td>86.48/79.18</td>
    <td>75.43/73.05</td>
  </tr>
  <tr>
    <td>VLSP_2018</td>
    <td>84.70/82.29</td>
    <td>75.23/76.48</td>
  </tr>
</table>