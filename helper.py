from tensorflow.keras.callbacks import EarlyStopping,LearningRateScheduler
from tensorflow.keras.metrics import Recall, Precision
import tensorflow_addons as tfa
import tensorflow as tf
from .processing_data import process_text, convert_text_to_number_sequence

class CustomCallback(EarlyStopping):
    def get_monitor_value(self, logs):
        logs = logs or {}
        monitor_value = (logs.get('val_aspect_f1_score')+logs.get('val_polarity_f1_score'))/2
        return monitor_value

def callbacks(scheduler,patience):
    return [LearningRateScheduler(scheduler),CustomCallback(patience=patience,restore_best_weights=True,mode='max')]

def metrics(num_aspect,num_polarity):
    return [[tfa.metrics.F1Score(num_classes=num_aspect,threshold=0.5,average='micro'),Recall(thresholds=0.5,name='recall'),Precision(thresholds=0.5,name='precision')],
               [tfa.metrics.F1Score(num_classes=num_polarity,threshold=0.5,average='micro'),Recall(thresholds=0.5,name='recall'),Precision(thresholds=0.5,name='precision')]]

def compute_metrics(y_true,y_pre,num=1):
    n = y_true.shape[-1]//num
    result = {}
    result['f1_score'] = []
    result['recall'] = []
    result['precision'] = []
    for i in range(n):
        recall = Recall()
        precision = Precision()
        f1 = tfa.metrics.F1Score(num_classes=num, threshold=0.5, average='micro')
        result['f1_score'].append(f1(y_true[:, i:i + num], y_pre[:, i:i + num]).numpy())
        result['recall'].append(recall(y_true[:, i:i + num], y_pre[:, i:i + num]).numpy())
        result['precision'].append(precision(y_true[:, i:i + num], y_pre[:, i:i + num]).numpy())
    return result

def test(model,x,vocab,bpe,aspect,polarity,annotator):
    sequences = process_text(x,annotator)
    X, X_mask, X_mask_1,_  = convert_text_to_number_sequence(sequences, vocab, bpe)
    y_aspect,y_polarity = model.predict([X, X_mask, X_mask_1])
    aspect = dict((y,x) for x,y in aspect.items())
    polarity = dict((y, x) for x, y in polarity.items())
    indicies_aspect = tf.where(tf.greater(y_aspect, 0.5))
    indicies_polarity = tf.where(tf.greater(y_polarity, 0.5))
    result_aspect = [[] for _ in range(len(x))]
    result_polarity = [[] for _ in range(len(x))]
    for i in range(indicies_aspect.shape[0]):
        result_aspect[int(indicies_aspect[i,0])].append(aspect[int(indicies_aspect[i,1])])
    for i in range(indicies_polarity.shape[0]):
        result_polarity[int(indicies_polarity[i,0])].append(aspect[int(indicies_polarity[i,1]//3)]+'#'+polarity[int(indicies_polarity[i,1]%3)])
    return result_aspect,result_polarity