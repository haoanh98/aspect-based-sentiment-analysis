import tensorflow as tf
import tensorflow_addons as tfa
from tensorflow.keras import layers, Model
from transformers import RobertaConfig,TFRobertaModel
import numpy as np
import logging
from gensim.models import Word2Vec

def build_embedding_matrix_word2vec(corpus,embed_dim,num_words,vocab):
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    model = Word2Vec(corpus, size=embed_dim, window=10, min_count=1, workers=5, sg=1)
    embedding_matrix = np.zeros((num_words, embed_dim))
    for word in model.wv.vocab:
        try:
            i = vocab.indices[word]
        except:
            i = vocab.indices['<unk>']
        embedding_matrix[i] = model.wv[word]
    return embedding_matrix

def get_angles(pos, i, d_model):
    angle_rates = 1 / np.power(10000, (2 * (i//2)) / np.float32(d_model))
    return pos * angle_rates

def positional_encoding(position, d_model):
    angle_rads = get_angles(np.arange(position)[:, np.newaxis],
                          np.arange(d_model)[np.newaxis, :],
                          d_model)
    # apply sin to even indices in the array; 2i
    angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])
    # apply cos to odd indices in the array; 2i+1
    angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])
    return angle_rads

class EncoderLayer(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1,**kwargs):
        super(EncoderLayer, self).__init__(**kwargs)
        self.att = tfa.layers.MultiHeadAttention(num_heads=num_heads, head_size=embed_dim//num_heads)
        self.ffn = tf.keras.Sequential(
            [layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim),]
        )
        self.layernorm1 = layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)

    def call(self, inputs, training, mask):
        attn_output = self.att([inputs, inputs],mask=mask)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.layernorm2(out1 + ffn_output)

class TokenAndPositionEmbedding(layers.Layer):
    def __init__(self, maxlen, vocab_size, embed_dim, embedding_matrix,**kwargs):
        super(TokenAndPositionEmbedding, self).__init__(**kwargs)
        self.token_emb = layers.Embedding(input_dim=vocab_size, output_dim=embed_dim,weights=[embedding_matrix],trainable=True)
        self.pos_emb = layers.Embedding(input_dim=maxlen, output_dim=embed_dim,weights=[positional_encoding(maxlen,embed_dim)],trainable=False)

    def call(self, x):
        maxlen = tf.shape(x)[-1]
        positions = tf.range(start=0, limit=maxlen, delta=1)
        positions = self.pos_emb(positions)
        x = self.token_emb(x)
        return x + positions

class TransformerBlock(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, maxlen, vocab_size ,embedding_matrix ,num_layers,rate=0.1,**kwargs):
        super(TransformerBlock, self).__init__(**kwargs)
        self.num_layers = num_layers
        self.embedding = TokenAndPositionEmbedding(maxlen, vocab_size, embed_dim, embedding_matrix)
        self.enc_layers = [EncoderLayer(embed_dim, num_heads, ff_dim,rate) for _ in range(num_layers)]
        self.GlobalAveragePooling = layers.GlobalAveragePooling1D()

    def call(self, inputs, training, mask):
        x = self.embedding(inputs)
        for i in range(self.num_layers):
          x = self.enc_layers[i](x, training, mask)
        return self.GlobalAveragePooling(x)

def creat_model(num_heads,ff_dim,num_layers,maxlen,embed_dim,num_words,embedding_matrix,num_aspect,num_polarity,path):
    config = RobertaConfig.from_pretrained(
        path + '/config.json',
        output_hidden_states=True
    )
    model_bert = TFRobertaModel.from_pretrained(path+'/model.bin', config=config, from_pt=True, name='phoBert')
    input_ids = layers.Input(shape=(maxlen,), name='input_token', dtype='int32')
    input_mask = layers.Input(shape=(maxlen,), name='mask_token_phoBert', dtype='int32')
    input_mask_1 = layers.Input(shape=(maxlen,maxlen,), name='mask_token_transformers', dtype='int32')
    transformer_block = TransformerBlock(embed_dim, num_heads, ff_dim, maxlen, num_words, embedding_matrix, num_layers,name='Transfomers')
    y = transformer_block(input_ids,True,input_mask_1)
    phobert = model_bert(input_ids, attention_mask=input_mask)[2]
    x = layers.Concatenate(name ='concatenate')([layers.Lambda(lambda x: x[-1][:,0,:],name='CLS_hidden_states_12')(phobert),
                                                 layers.Lambda(lambda x: x[-2][:,0,:],name='CLS_hidden_states_11')(phobert),
                                                 layers.Lambda(lambda x: x[-3][:,0,:],name='CLS_hidden_states_10')(phobert),
                                                 layers.Lambda(lambda x: x[-4][:,0,:],name='CLS_hidden_states_9')(phobert),
                                                 y])
    dropout_layer = layers.Dropout(0.2, name ='dropout' )(x)
    output_aspect = layers.Dense(num_aspect, activation='sigmoid',name = 'aspect')(dropout_layer)
    output_polarity = layers.Dense(num_polarity, activation='sigmoid',name = 'polarity')(dropout_layer)
    model = Model([input_ids,input_mask,input_mask_1], [output_aspect,output_polarity])
    return model