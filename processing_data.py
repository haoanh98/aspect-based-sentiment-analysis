# Xử lý content
import re
import numpy as np
from tqdm import tqdm
import pandas as pd
import json

def replace_emojis(content):
    emoji_happy = [r"\U0001F600", r"\U0001F601", r"\U0001F602", r"\U0001F603", r"\U0001F604", r"\U0001F605", r"\U0001F606",
                   r"\U0001F607", r"\U0001F609",
                   r"\U0001F60A", r"\U0001F642", r"\U0001F643", r"\U0001F923", r"\U0001F970", r"\U0001F60D", r"\U0001F929",
                   r"\U0001F618", r"\U0001F617",
                   r"\U0001F61A", r"\U0001F619", r"\U0001F972", r"\U0001F60B", r"\U0001F61B", r"\U0001F61C",
                   r"\U0001F92A",
                   r"\U0001F61D", r"\U0001F911", r"\U0001F917", r"\U0001F92D", r"\U0001F92B", r"\U0001F914", r"\U0001F910",
                   r"\U0001F928", r"\U0001F610", r"\U0001F611",
                   r"\U0001F636", r"\U0001F60F", r"\U0001F612", r"\U0001F644", r"\U0001F62C", r"\U0001F925", r"\U0001F60C",
                   r"\U0001F614", r"\U0001F62A",
                   r"\U0001F924", r"\U0001F634", r"\U0001F920", r"\U0001F973", r"\U0001F978", r"\U0001F60E", r"\U0001F913",
                   r"\U0001F9D0"]

    emoji_sad = [r"\U0001F637", r"\U0001F912", r"\U0001F915", r"\U0001F922", r"\U0001F92E", r"\U0001F927", r"\U0001F975",
                 r"\U0001F976", r"\U0001F974",
                 r"\U0001F635", r"\U0001F92F", r"\U0001F615", r"\U0001F61F", r"\U0001F641", r"\U0001F62E",
                 r"\U0001F62F", r"\U0001F632",
                 r"\U0001F633", r"\U0001F97A", r"\U0001F626", r"\U0001F627", r"\U0001F628", r"\U0001F630", r"\U0001F625",
                 r"\U0001F622", r"\U0001F62D",
                 r"\U0001F631", r"\U0001F616", r"\U0001F623", r"\U0001F61E", r"\U0001F613", r"\U0001F629", r"\U0001F62B",
                 r"\U0001F971",
                 r"\U0001F624", r"\U0001F621", r"\U0001F620", r"\U0001F92C", r"\U0001F608", r"\U0001F47F", r"\U0001F480"]

    words = content.split()
    reformed = []
    for w in words:
        if w in emoji_happy:
            reformed.append("vui")
        elif w in emoji_sad:
            reformed.append("buồn")
        else:
            reformed.append(w)
    result = " ".join(reformed)
    return result

def delete_emojis(content):
    emoji_pattern = re.compile("["
                               u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                               u"\U0001F600-\U0001F64F"  # emoticons
                               u"\U0001F680-\U0001F6FF"  # transport & map symbols
                               u"\U0001F700-\U0001F77F"  # alchemical symbols
                               u"\U0001F780-\U0001F7FF"  # Geometric Shapes Extended
                               u"\U0001F800-\U0001F8FF"  # Supplemental Arrows-C
                               u"\U0001F900-\U0001F9FF"  # Supplemental Symbols and Pictographs
                               u"\U0001FA00-\U0001FA6F"  # Chess Symbols
                               u"\U0001FA70-\U0001FAFF"  # Symbols and Pictographs Extended-A
                               u"\U00002702-\U000027B0"  # Dingbats
                               u"\U00002702-\U000027B0"
                               u"\U000024C2-\U0001F251"
                               u"\U0001f926-\U0001f937"
                               u'\U00010000-\U0010ffff'
                               u"\u200d"
                               u"\u2640-\u2642"
                               u"\u2600-\u2B55"
                               u"\u23cf"
                               u"\u23e9"
                               u"\u231a"
                               u"\u3030"
                               u"\ufe0f"
                               "]+", flags=re.UNICODE)
    return emoji_pattern.sub(r'', content)

def replace_smileys(content):
    emoticons_happy = set(
        [':-)', ':)', ';)', ':o)', ':]', ':3', ':c)', ':>', '=]', '8)', '=)', ':}', ':D', '=)))', '=))', '=))))',
         '=))))))'
         ':^)', ':-D', ':D', '8-D', '8D', 'x-D', 'xD', 'X-D', 'XD', '=-D', '=D',
         '=-3', '=3', ':-))', ":'-)", ":')", ':*', ':^*', '>:P', ':-P', ':P', 'X-P',
         'x-p', 'xp', 'XP', ':-p', ':p', '=p', ':-b', ':b', '>:)', '>;)', '>:-)', '<3'])

    emoticons_sad = set([':L', ':-/', '>:/', ':S', '>:[', ':@', ':-(', ':[', ':-||', '=L', ':<',
                         ':-[', ':-<', '=\\', '=/', '>:(', ':(', '>.<', ":'-(", ":'(", ':\\', ':-c',
                         ':c', ':{', '>:\\', ';('])

    words = content.split()
    reformed = []
    for w in words:
        if w in emoticons_happy:
            reformed.append("vui")
        elif w in emoticons_sad:
            reformed.append("buồn")
        else:
            reformed.append(w)
    result = " ".join(reformed)
    return result

def normalize(content):
    result = replace_emojis(content)
    result = delete_emojis(result)
    result = replace_smileys(result)
    return result.strip()

def process_text(squences,annotator):
    result = []
    for content in squences:
        content = annotator.tokenize(normalize(content))
        content = [word for sentence in content for word in sentence]
        content = " ".join(content)
        result.append(content)
    return result

def load_json(path):
    f = open(path, "r")
    return json.loads(f.read())

def load_data(path,aspect,polarity,num_aspect,num_polarity):
    data = pd.read_csv(path)
    x = list(data.content)
    label_aspect = list(data.label_aspect)
    label_polarity = list(data.label_polarity)
    y_polarity = []
    y_aspect = []
    for aspect_,polarity_ in zip(label_aspect,label_polarity):
        a = aspect_.strip().split()
        try:
            b = polarity_.strip().split()
        except:
            b = []
        y_polarity.append(np.zeros(num_polarity))
        y_aspect.append(np.zeros(num_aspect))
        for i,j in zip(a, b):
            y_polarity[-1][aspect[i]*3+polarity[j]] = 1
            y_aspect[-1][aspect[i]] = 1
        if len(a)!=len(b):
            y_aspect[-1][10] = 1
    y_polarity = np.array(y_polarity)
    y_aspect = np.array(y_aspect)
    return x,y_aspect,y_polarity

def convert_text_to_number_sequence(sequences,vocab,bpe,max_sequence_length=256):
    # Khởi tạo ma trận output
    outputs = np.zeros((len(sequences), max_sequence_length), dtype=np.int32)
    mask_token = np.ones((len(sequences), max_sequence_length), dtype=np.int32)
    mask_token_1 = np.ones((len(sequences), max_sequence_length,max_sequence_length))
    # Index của các token cls (đầu câu), eos (cuối câu), padding (padding token)
    # cls_id = 0
    eos_id = 2
    pad_id = 1
    corpus = []
    for idx, row in tqdm(enumerate(sequences), total=len(sequences)):
        # Mã hóa subwords theo byte pair encoding(bpe)
        subwords = '<s> '+ bpe.encode(row)+ ' </s>'
        corpus.append(subwords.split())
        input_ids = vocab.encode_line(subwords, append_eos=False, add_if_not_exist=False).long().tolist()
        # Truncate input nếu độ dài vượt quá max_seq_len
        if len(input_ids) > max_sequence_length:
            input_ids = input_ids[:max_sequence_length]
            input_ids[-1] = eos_id
        else:
            # Padding nếu độ dài câu chưa bằng max_seq_len
            length_ = len(input_ids)
            input_ids = input_ids + [pad_id, ]*(max_sequence_length - len(input_ids))
            mask = [0 if i==1 else 1 for i in input_ids]
            mask_token_1[idx,:,length_:] = 0
            mask_token_1[idx,length_:,:] = 0
            mask_token[idx,:] = np.array(mask)
        outputs[idx,:] = np.array(input_ids)
    return outputs, mask_token, mask_token_1, corpus